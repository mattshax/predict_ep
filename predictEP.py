
import traceback
import weka.core.jvm as jvm
import wekaexamples.helper as helper
from weka.core.database import InstanceQuery
from weka.classifiers import Classifier
from weka.core.dataset import Attribute
from weka.core.dataset import Instance
from weka.core.dataset import Instances

def main():
    
    # import the training dataset from the postgres db
    helper.print_title("Loading energy data from postgres database")
    iquery = InstanceQuery()
    iquery.db_url = "jdbc:postgresql://172.17.1.141:5432/postgres"
    iquery.user = "postgres"
    iquery.password = "postgres"
    iquery.query = "select orientation,sawtooth_count,sawtooth_angle,n_direction,s_direction,e_direction,w_direction,glazing_type,total_annual_eui from ep_results"
    data = iquery.retrieve_instances()
    data.class_is_last()
    #print(data)
    
    # create a lazy-ibk classifier
    c1 = Classifier(classname='weka.classifiers.lazy.IBk', options=["-K","1"])
    c2 = Classifier(classname='weka.classifiers.lazy.IBk', options=["-K","2"])
    c1.build_classifier(data)
    c2.build_classifier(data)
    print(c2)
    
    # create test dataset attributes
    orientation = Attribute.create_numeric("orientation")
    sawtooth_count = Attribute.create_numeric("sawtooth_count")
    sawtooth_angle = Attribute.create_numeric("sawtooth_angle")
    n_direction = Attribute.create_numeric("n_direction")
    s_direction = Attribute.create_numeric("s_direction")
    e_direction = Attribute.create_numeric("e_direction")
    w_direction = Attribute.create_numeric("w_direction")
    glazing_type = Attribute.create_numeric("glazing_type")
    total_annual_eui = Attribute.create_numeric("total_annual_eui")

    # create test dataset
    dataset = Instances.create_instances("test_data",[orientation,sawtooth_count,sawtooth_angle,n_direction,s_direction,e_direction,w_direction,glazing_type,total_annual_eui],0)
    
    # append the value to test dataset
    test_values=[-15,0,10,0,0,0,0,19,0]
    inst = Instance.create_instance(test_values)
    dataset.add_instance(inst)
    dataset.class_is_last()
    
    # run the predictions for the test values
    for index, inst in enumerate(dataset):
        print("testing: "+str(inst))
        pred1 = c1.classify_instance(inst)
        pred2 = c2.classify_instance(inst)
        print("prediction (k=2): "+str(round(pred2,2))+" kWh/m2/yr")
        print ""
        print("closest actual (k=1): "+str(round(pred1,2))+" kWh/m2/yr")
        print("distance from closet actual: "+str(round(abs(pred2-pred1),2))+" kWh/m2/yr")

if __name__ == "__main__":
    try:
        jar = "/core/weka/postgres/lib/postgresql-9.4.1207.jar"
        jvm.start(class_path=[jar])
        main()
    except Exception, e:
        print(traceback.format_exc())
    finally:
        jvm.stop()
